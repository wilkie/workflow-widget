"use strict";

import Workflow from "../../src/workflow.js";

var graph = {
    "center": {
        "x": 50,
        "y": 200
    },
    "connections": [
        {
            "inputs": [
                {
                    "connections": [
                        {
                            "to": {
                                "node": 1,
                                "port": 0
                            }
                        }
                    ],
                    "name": "src",
                    "type": " ",
                    "visibility": "visible",
                    //"max": 1
                },
                {
                    "connections": [
                        {
                            "to": {
                                "node": 3,
                                "port": 0
                            }
                        }
                    ],
                    "name": "General Options",
                    "type": "configuration",
                    "visibility": "hidden",
                    "max": 1
                }
            ],
            "outputs": [
                {
                    "connections": [],
                    "name": "cache_link",
                    "type": "memHierarchy.memEvent",
                    "visibility": "visible"
                }
            ],
            "ports": [
                {
                    "name": "moo",
                    "type": "bar"
                }
            ],
            "type": "Component",
            "name": "Miranda",
            "data": {
                "component-name": "cpu",
                "component-type": "miranda.BaseCPU"
            }
        },
        {
            "icon": "images/simulator.svg",
            "inputs": [
                {
                    "connections": [],
                    "name": "low_network_0",
                    "type": "memHierarchy.MemEventBase",
                    "visibility": "visible"
                }
            ],
            "outputs": [
                {
                    "connections": [],
                    "name": "high_network_0",
                    "type": "memHierarchy.MemEventBase",
                    "visibility": "visible"
                }
            ],
            "name": "Cache",
            "type": "Component",
            "position": {"y": 300},
            "data": {
                "component-name": "l1cache",
                "component-type": "memHierarchy.Cache"
            }
        },
        ///
        {
            "inputs": [
                {
                    "connections":[ ],
                    "name":"direct_link",
                    "type":"memHierarchy.MemEventBase",
                    "visibility":"visible"
                },
                {
                    "connections":[],
                    "name":"network",
                    "type":" "
                },
                {
                    "connections":[],
                    "name":"network_ack",
                    "type":" ",
                    "visibility":"hidden"
                },
                {
                    "connections":[],
                    "name":"network_fwd",
                    "type":" ",
                    "visibility":"hidden"
                },
                {
                    "connections":[],
                    "name":"network_data",
                    "type":" ",
                    "visibility":"hidden"
                }
            ],
            "ports": [
                {
                    "name": "foo",
                    "type": "bar"
                }
            ],
            "name": "MemController",
            "type": "Component",
            "icon": "images/simulator.svg",
            "position": {"y": 150},
            "data": {
                "component-name": "memory",
                "component-type": "memHierarchy.MemController"
            }
        },//*/
        {
            "icon": "images/configuration.svg",
            "inputs": [ ],
            "outputs": [
                {
                    "connections":[ 
                        {
                            "to": {
                                "node": 0,
                                "port": 1
                            }
                        }
                    ],
                    "name":"self",
                    "type":"configuration",
                    "visibility":"visible",
                    "max": 1
                }
            ],
            "visibility":"hidden",
            "ports": [ ],
            "type": "configuration",
            "name": "General Options",
        },
    ]
};

(function bootstrap() {
    let workflow_element = document.querySelectorAll("workflow-widget")[0];
    let workflow = new Workflow(workflow_element, {
        buttons: [
            {
                classes: ["view-button"],
                inactive: {
                    icon: "https://occam.software/images/dynamic/hex/5999a6/icons/ui/search.svg"
                },
                hover: {
                    icon: "https://occam.software/images/dynamic/hex/ffffff/icons/ui/search.svg"
                }
            },
            {
                classes: ["configure-button"],
                inactive: {
                    icon: "https://occam.software/images/dynamic/hex/5999a6/icons/objects/configuration.svg"
                },
                hover: {
                    icon: "https://occam.software/images/dynamic/hex/ffffff/icons/objects/configuration.svg"
                }
            },
            {
                classes: ["edit-button"],
                inactive: {
                    icon: "https://occam.software/images/dynamic/hex/5999a6/icons/ui/edit.svg"
                },
                hover: {
                    icon: "https://occam.software/images/dynamic/hex/ffffff/icons/ui/edit.svg"
                }
            }
        ]
    }, graph);

    workflow.on("button-click", (event) => {
        console.log(event);
    });

    for (let i = 0; i < 10; i++) {
        workflow.nodes[0].updateJob({id: i, status: "started"});
    }

    let index = 0;
    window.setInterval( () => {
        if (index < 10) {
            workflow.nodes[0].updateJob({id: index, status: "finished"});
        }
        index++;
    }, 500);

    let index2 = 100;
    window.setInterval( () => {
        if (index < 200) {
            let choice = Math.floor(Math.random() * 100) % 10;
            let status = ["finished", "finished", "finished", "failed", "failed", "failed", "failed", "finished", "finished", "finished"][choice];
            workflow.nodes[1].updateJob({id: index2, status: status });
        }
        index2++;
    }, 200);

    for (let i = 100; i < 200; i++) {
            workflow.nodes[1].updateJob({id: i, status: "started"});
    }
})();
